<?php

/**
 * @file
 * Calculation logic migration dashboard.
 */

/**
 * Migration process Labels and its score.
 */
define('MIGRATION_ADVISOR_ALMOST_AUTO_LABEL', 'Almost Auto');
define('MIGRATION_ADVISOR_ALMOST_AUTO_SCORE', 80);

define('MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL', 'Partial Auto');
define('MIGRATION_ADVISOR_PARTIAL_AUTO_SCORE', 60);

define('MIGRATION_ADVISOR_MANUAL_LABEL', 'Manual');
define('MIGRATION_ADVISOR_MANUAL_SCORE', 40);

define('MIGRATION_ADVISOR_SAME_SERVER_LABEL', 'Same Server and Same Environment');
define('MIGRATION_ADVISOR_SAME_SERVER_SCORE', 70);
define('MIGRATION_ADVISOR_SAME_SERVER_GRADE', 'A');

define('MIGRATION_ADVISOR_DIFF_SERVER_LABEL', 'Different Server and Same Environment');
define('MIGRATION_ADVISOR_DIFF_SERVER_SCORE', 60);
define('MIGRATION_ADVISOR_DIFF_SERVER_GRADE', 'B');

define('MIGRATION_ADVISOR_DIFF_ENVIRON_LABEL', 'Different Environment');
define('MIGRATION_ADVISOR_DIFF_ENVIRON_SCORE', 40);
define('MIGRATION_ADVISOR_DIFF_ENVIRON_GRADE', 'C');

define('MIGRATION_ADVISOR_EARLY_LABEL', 'Very early to decide');
define('MIGRATION_ADVISOR_EARLY_SCORE', 40);
define('MIGRATION_ADVISOR_EARLY_GRADE', '-');


define('MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL', 'Not Applicable');
define('MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL_SCORE', 0);

/**
 * Complexity grades.
 */
define('MIGRATION_ADVISOR_COMPLEX_GRADE', 'C');
define('MIGRATION_ADVISOR_MODERATE_GRADE', 'B');
define('MIGRATION_ADVISOR_LOW_GRADE', 'A');

/**
 * Fetch initial structure of json from local.
 */
function migration_advisor_get_data_structure_from_json() {
  $module_path = drupal_get_path('module', 'migration_advisor');
  $json_file_path = $module_path . '/structure.json';
  $json_str = file_get_contents($json_file_path);
  $data = json_decode($json_str, TRUE);
  return $data;
}

/**
 * First Page in the module.
 */
function migration_advisor_initial_form($form, &$form_state) {
  $form = array();
  $report_generated = FALSE;
  if (migration_advisor_get_projects_available()) {
    $report_generated = TRUE;
  }
  $assessment_msg = 'To complete initial assessment. ';
  $fetch_failed_flag = variable_get('migration_advisor_fetch_failed_flag');
  if (isset($fetch_failed_flag)) {
    if ($fetch_failed_flag) {
      $report_generated = FALSE;
      $assessment_msg = 'Initial assessment incomplete please try again.';
    }
  }
  $form['migration_advisor_container'] = array(
    '#type' => 'container',
    '#prefix' => '<div class="migration-form-container">',
    '#suffix' => '</div>',
  );
  $form['migration_advisor_container']['button'] = array(
    '#type' => '#markup',
    '#prefix' => filter_xss_admin(
    '<div class="mg-button"><div class="get-assessment not-done">'
    . $assessment_msg . l(t('Click here'),
    'admin/reports/updates/upgradeadvise/check',
    array('query' => drupal_get_destination())) . '</div></div>'),
  );

  if ($report_generated) {
    $form['migration_advisor_container']['button']['#prefix'] =
    filter_xss_admin('<div><div class="get-assessment done">Initial Assessment done.</div>');
  }

  $form['migration_advisor_container2'] = array(
    '#type' => 'container',
    '#prefix' => '<div class="migration-form-container2">',
    '#suffix' => '</div>',
  );

  variable_del('migration_advisor_migration_version');
  $form['migration_advisor_container2']['migration_advisor_migration_version'] = array(
    '#type' => 'select',
    '#title' => t('Target version of Drupal core'),
    '#options' => drupal_map_assoc(array('8.x', '9.x')),
    '#required' => TRUE,
    '#default_value' => variable_get('migration_advisor_migration_version', MIGRATION_ADVISOR_CORE_VERSION),
    '#description' => t('Select the version of Drupal core you wish to analyse.'),
  );
  $options = array(
    MIGRATION_ADVISOR_SAME_SERVER_GRADE => MIGRATION_ADVISOR_SAME_SERVER_LABEL,
    MIGRATION_ADVISOR_DIFF_SERVER_GRADE => MIGRATION_ADVISOR_DIFF_SERVER_LABEL,
    MIGRATION_ADVISOR_DIFF_ENVIRON_GRADE => MIGRATION_ADVISOR_DIFF_ENVIRON_LABEL,
    MIGRATION_ADVISOR_EARLY_GRADE => MIGRATION_ADVISOR_EARLY_LABEL,
  );
  $form['migration_advisor_container2']['radio'] = array(
    '#type' => 'radios',
    '#title' => t('Migration Information'),
    '#options' => $options,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('To get final Evaluation Report'),
    '#disabled' => !$report_generated,
    '#prefix' => '<div class="mg-button">',
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Initial Form submit.
 */
function migration_advisor_initial_form_submit($form, &$form_state) {
  $form_data = array(
    'radio' => $form_state['values']['radio'],
  );

  variable_set('migration_advisor_migration_version', $form_state['values']['migration_advisor_migration_version']);

  $form_state['redirect'] = array('admin/reports/migrationadvisor',
  array('query' => $form_data),
  );
}

/**
 * Migration Dashboard Initial page.
 */
function migration_advisor_migration_dashboard() {

  $data = migration_advisor_get_data_structure_from_json();

  // Initialise data object for configuration table.
  migration_advisor_initialise_config_table_rows($data);

  // Initialise data object for main table.
  migration_advisor_initialise_main_table_rows($data);

  // TODO: Config data prefill.
  migration_advisor_get_web_hosting_info($data);

  // TODO: Main table
  // 1. Get Core, contributed, custom module counts and append to data array.
  migration_advisor_get_module_count($data);
  migration_advisor_get_contrib_count($data);
  // 2. Get themes count and append to data.
  migration_advisor_get_themes_count($data);
  // 3. Get views count and append to data.
  migration_advisor_get_custom_views_count($data);
  // 4. Get webform count and append to data.
  migration_advisor_get_webform_count($data);
  // 5. Get Custom blocks count and append data.
  migration_advisor_get_custom_blocks_count($data);
  // 6. Get Content count and append data.
  migration_advisor_get_node_count($data);
  // 7. Get Menu count and append data.
  migration_advisor_get_menu_count($data);
  // 8. Get Users count and append data.
  migration_advisor_get_users_count($data);
  // 9. Get Media count and append data.
  migration_advisor_get_media_files($data);
  // 10. Update Environment Grade.
  migration_advisor_get_environment_score($data);
  // 11. Calculate overall grade.
  migration_advisor_get_overall_grade($data);
  // 12. Update count of grades.
  migration_advisor_update_grade_count($data);

  return theme('migration_advisor_dashboard', array('data' => $data));
}

/**
 * Initialise the main table data counts.
 */
function migration_advisor_initialise_main_table_rows(&$data) {

  $main_table_data = array();
  $main_table_columns_keys = array();

  foreach ($data['main_table']['column_names'] as $key => $val) {
    if ($key != 'type_of_modules') {
      $main_table_columns_keys[$key] = 0;
    }
  }
  foreach ($data['main_table']['row_names'] as $key => $val) {
    $main_table_data[$key] = $main_table_columns_keys;
  }

  $data['main_table']['data'] = $main_table_data;

}

/**
 * Initialise the config table data counts.
 */
function migration_advisor_initialise_config_table_rows(&$data) {

  $config_data = array();

  foreach ($data['configuration']['row_names'] as $key => $val) {
    $config_data[$key] = 0;
  }

  $data['configuration']['data'] = $config_data;

}

/**
 * Associate Task to its  migration process level.
 */
function migration_advisor_get_tasks_migration_process_levels($task = 0) {
  $options = array(
    'core' => MIGRATION_ADVISOR_ALMOST_AUTO_LABEL,
    'contributed' => MIGRATION_ADVISOR_MANUAL_LABEL,
    'custom' => MIGRATION_ADVISOR_MANUAL_LABEL,
    'themes' => MIGRATION_ADVISOR_MANUAL_LABEL,
    'views' => MIGRATION_ADVISOR_MANUAL_LABEL,
    'webforms' => MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL,
    'custom_blocks' => MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL,
    'content' => MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL,
    'menu' => MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL,
    'user_roles' => MIGRATION_ADVISOR_ALMOST_AUTO_LABEL,
    'users' => MIGRATION_ADVISOR_ALMOST_AUTO_LABEL,
    'media_files' => MIGRATION_ADVISOR_MANUAL_LABEL,
    'environment' => MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL,
  );
  if (empty($task)) {
    return $options;
  }
  return $options[$task];
}

/**
 * Associate Process level to its score.
 */
function migration_advisor_get_score($migration_process = 0) {
  $options = array(
    MIGRATION_ADVISOR_ALMOST_AUTO_LABEL => MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL,
    MIGRATION_ADVISOR_PARTIAL_AUTO_LABEL => MIGRATION_ADVISOR_PARTIAL_AUTO_SCORE,
    MIGRATION_ADVISOR_MANUAL_LABEL => MIGRATION_ADVISOR_MANUAL_SCORE,
  );
  if (empty($migration_process)) {
    return $options;
  }
  return $options[$migration_process];
}

/**
 * Fetch Complexity grade.
 */
function migration_advisor_get_complexity($task_type, $active_count) {

  $no_grade_label = '-';
  $consider_for_eval = migration_advisor_get_evaluate_grade_status($task_type);
  if ($consider_for_eval == 0) {
    return $no_grade_label;
  }
  // As grades for contribution and environment is calculated separately.
  if ($task_type == 'contrib' || $task_type == 'environment') {
    return $no_grade_label;
  }
  else {

    if ($active_count == 0) {
      return MIGRATION_ADVISOR_LOW_GRADE;
    }
    $migration_process = migration_advisor_get_tasks_migration_process_levels($task_type);
    $score_val = migration_advisor_get_score($migration_process);
    if (is_numeric($score_val)) {
      if ($score_val == MIGRATION_ADVISOR_MANUAL_SCORE) {
        return MIGRATION_ADVISOR_COMPLEX_GRADE;
      }
      elseif ($score_val == MIGRATION_ADVISOR_PARTIAL_AUTO_SCORE) {
        return MIGRATION_ADVISOR_MODERATE_GRADE;
      }
    }
    elseif ($score_val == MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL) {
      return $no_grade_label;
    }
  }

}

/**
 * Custom blocks count.
 */
function migration_advisor_get_custom_blocks_count(&$data) {

  // Fetch custom modules names:
  $modules_data = migration_advisor_get_modules_details();
  $all_modules = array();
  if (isset($modules_data['custom'])) {
    foreach ($modules_data['custom'] as $custom_mod) {
      if ($custom_mod['status'] == 1) {
        $all_modules[] = $custom_mod['machinename'];
      }
    }
  }
  $all_modules[] = 'views';
  $all_modules[] = 'block';
  // Query the count of blocks.
  $activetheme = variable_get('theme_default');
  $query = db_select('block', 'b');
  $query->fields("b", array("status"));
  $query->addExpression('count(1)', 'statuscount');
  $query->condition("module", $all_modules, 'IN');
  $result = $query->condition("theme", $activetheme)
        // ->condition($db_or)
    ->groupBy('b.status')
    ->execute()->fetchAllkeyed();

  $custom_block_counts = 0;
  $custom_block_active = 0;
  $custom_block_inactive = 0;

  foreach ($result as $block_status => $block_count) {
    if ($block_status == 0) {
      $custom_block_inactive = $block_count;
    }
    elseif ($block_status == 1) {
      $custom_block_active = $block_count;
    }
  }
  $custom_block_counts = $custom_block_active + $custom_block_inactive;

  $not_higher_version_compatible = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $impact_on_performance = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $migration_complexity = migration_advisor_get_complexity('custom_blocks', $custom_block_active);

  $custom_blocks = $data['main_table']['data']['custom_blocks'];
  $custom_blocks['total_count'] = $custom_block_counts;
  $custom_blocks['active'] = $custom_block_active;
  $custom_blocks['not_active'] = $custom_block_inactive;
  $custom_blocks['not_higher_version_compatible'] = $not_higher_version_compatible;
  $custom_blocks['impact_on_performance'] = $impact_on_performance;
  $custom_blocks['migration_complexity'] = $migration_complexity;
  if ($custom_blocks['migration_complexity'] == '-') {
    $tool_tip_content = t('Not considered as no custom blocks used.');
  }
  elseif ($custom_blocks['migration_complexity'] == MIGRATION_ADVISOR_LOW_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    No active custom blocks present.', array(
      '@grade_p' => $custom_blocks['migration_complexity'],
    ));
  }
  elseif ($custom_blocks['migration_complexity'] == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Considerable amount of blocks present to migrate.', array(
      '@grade_p' => $custom_blocks['migration_complexity'],
    ));
  }
  elseif ($custom_blocks['migration_complexity'] == MIGRATION_ADVISOR_COMPLEX_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Considerable amount of blocks present to migrate.', array(
      '@grade_p' => $custom_blocks['migration_complexity'],
    ));
  }
  $custom_blocks['complexity_desc'] = $tool_tip_content;
  $data['main_table']['data']['custom_blocks'] = $custom_blocks;

  // Update graph data:
  $data['graph_data']['custom_blocks']['total_count'] = $custom_blocks['total_count'];
  $data['graph_data']['custom_blocks']['active'] = $custom_blocks['active'];
  $data['graph_data']['custom_blocks']['inactive'] = $custom_blocks['not_active'];
  $data['graph_data']['custom_blocks']['active_percent'] =
  !empty($custom_blocks['total_count']) ? round((($custom_blocks['active'] / $custom_blocks['total_count']) * 100), 2) : 0;
  $data['graph_data']['custom_blocks']['inactive_percent'] =
  !empty($custom_blocks['total_count']) ? round((($custom_blocks['not_active'] / $custom_blocks['total_count']) * 100), 2) : 0;
  $data['graph_data']['custom_blocks']['grade'] = $custom_blocks['migration_complexity'];

}

/**
 * Get views count.
 */
function migration_advisor_get_custom_views_count(&$data) {

  $custom_views = $data['main_table']['data']['views'];
  $custom_views_disabled = 0;
  $custom_views_enabled = 0;

  // To get all views.
  if (module_exists('views')) {
    $views_in_use = views_get_all_views();

    foreach ($views_in_use as $value) {
      $view = views_get_view($value->name);
      if ($view->disabled) {
        $custom_views_disabled++;
      }
      else {
        $custom_views_enabled++;
      }
    }
  }

  $custom_views['total_count'] = isset($views_in_use) ? count($views_in_use) : 0;
  $custom_views['active'] = $custom_views_enabled;
  $custom_views['not_active'] = $custom_views_disabled;
  $custom_views['not_higher_version_compatible'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $custom_views['impact_on_performance'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $custom_views['migration_complexity'] = migration_advisor_get_complexity('views', $custom_views_enabled);
  if ($custom_views['migration_complexity'] == '-') {
    $tool_tip_content = t('Not considered as views module is not used.');
  }
  elseif ($custom_views['migration_complexity'] == MIGRATION_ADVISOR_LOW_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    No active views available to migrate.', array(
      '@grade_p' => $custom_views['migration_complexity'],
    ));
  }
  elseif ($custom_views['migration_complexity'] == MIGRATION_ADVISOR_COMPLEX_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Because manual effort to migrate views.', array(
      '@grade_p' => $custom_views['migration_complexity'],
    ));
  }
  $custom_views['complexity_desc'] = $tool_tip_content;
  $data['main_table']['data']['views'] = $custom_views;

  // Update graph data:
  $data['graph_data']['views']['total_count'] = $custom_views['total_count'];
  $data['graph_data']['views']['active'] = $custom_views['active'];
  $data['graph_data']['views']['inactive'] = $custom_views['not_active'];
  $data['graph_data']['views']['active_percent'] =
  !empty($custom_views['total_count']) ? round((($custom_views['active'] / $custom_views['total_count']) * 100), 2) : 0;
  $data['graph_data']['views']['inactive_percent'] =
  !empty($custom_views['total_count']) ? round((($custom_views['not_active'] / $custom_views['total_count']) * 100), 2) : 0;
  $data['graph_data']['views']['grade'] = $custom_views['migration_complexity'];
}

/**
 * To get details about Users and returning to data.
 */
function migration_advisor_get_users_count(&$data) {

  $total_no_active_user = db_select('users', 'u')
    ->fields('u')
    ->condition('uid', 0, '>')
    ->condition('status', 1, '=')
    ->countQuery()->execute()->fetchField();

  $total_no_inactive_user = db_select('users', 'u')
    ->fields('u')
    ->condition('uid', 0, '>')
    ->condition('status', 0, '=')
    ->countQuery()->execute()->fetchField();

  $total_no_users_roles = db_select('role', 'r')
    ->countQuery()->execute()->fetchField();

  $query = db_select('role', 'l');
  $query->leftjoin('users_roles', 'u', 'u.rid = l.rid');
  $query->addExpression('count(distinct(u.rid))', 'row_count');
  $total_no_active_users_role = $query->execute()->fetchField();

  $users_roles_details = $data['main_table']['data']['user_roles'];
  $users_roles_details['total_count'] = $total_no_users_roles;
  $users_roles_details['active'] = $total_no_active_users_role;
  $users_roles_details['not_active'] = $total_no_users_roles - $total_no_active_users_role;
  $users_roles_details['not_higher_version_compatible'] = t('Not Applicable');
  $users_roles_details['impact_on_performance'] = t('Not Applicable');
  $users_roles_details['migration_complexity'] = '-';
  $tool_tip_content = t('Not considered for complexity calculation as core migration
  takes care.');
  $users_roles_details['complexity_desc'] = $tool_tip_content;
  $data['main_table']['data']['user_roles'] = $users_roles_details;

  $users_details = $data['main_table']['data']['users'];
  $users_details['total_count'] = $total_no_active_user + $total_no_inactive_user;
  $users_details['active'] = $total_no_active_user;
  $users_details['not_active'] = $total_no_inactive_user;
  $users_details['not_higher_version_compatible'] = t('Not Applicable');
  $users_details['impact_on_performance'] = t('Not Applicable');
  $users_details['migration_complexity'] = '-';
  $tool_tip_content = t('Not considered for complexity calculation as core migration
  takes care.');
  $users_details['complexity_desc'] = $tool_tip_content;
  $data['main_table']['data']['users'] = $users_details;
}

/**
 * Fetch contributed module data.
 */
function migration_advisor_get_contrib_count(&$data) {

  $module_data = migration_advisor_get_modules_status_data();

  $migration_core_version = variable_get('migration_advisor_migration_version', MIGRATION_ADVISOR_CORE_VERSION);

  $contrib_data = $data['main_table']['data']['contribution'];
  if (empty($module_data)) {
    $contrib_data['not_higher_version_compatible'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
    $contrib_data['impact_on_performance'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
    $contrib_data['migration_complexity'] = '-';
  }
  else {
    $contrib_data['not_higher_version_compatible'] = 0;
    foreach ($module_data as $main_module_key => $main_module) {

      if ($main_module_key == 'drupal') {
        continue;
      }

      if (
            $main_module['module_status'] == MIGRATION_ADVISOR_ERROR_VAL ||
            $main_module['module_status'] == MIGRATION_ADVISOR_OBSOLETE_VAL ||
            $main_module['module_status'] == MIGRATION_ADVISOR_UNKNOWN_VAL
        ) {
        $contrib_data['not_higher_version_compatible'] = $contrib_data['not_higher_version_compatible'] + count($main_module['includes']);
      }
      elseif ($migration_core_version == MIGRATION_ADVISOR_CORE_VERSION_9) {
        $drupal_9_compatible = FALSE;
        if ($main_module['status'] == MIGRATION_ADVISOR_CORE) {
          $drupal_9_compatible = TRUE;
        }
        else {
          $drupal_9_compatible = $main_module['drupal_9_compatible'];
        }
        if (!$drupal_9_compatible) {
          $contrib_data['not_higher_version_compatible']++;
        }
      }
    }
    $grade = MIGRATION_ADVISOR_LOW_GRADE;
    if (!empty($contrib_data['not_higher_version_compatible'])) {
      $not_compatible_percent = round(($contrib_data['not_higher_version_compatible'] / $contrib_data['active']) * 100);
      $grade = migration_advisor_get_complexity_compatibility($not_compatible_percent);
      // Update graph data:
      $data['graph_data']['module_compatibility']['total_count'] = $contrib_data['total_count'];
      $data['graph_data']['module_compatibility']['active'] = $contrib_data['active'];
      $data['graph_data']['module_compatibility']['inactive'] = $contrib_data['not_active'];
      $data['graph_data']['module_compatibility']['active_percent'] =
      !empty($contrib_data['total_count']) ? round((($contrib_data['active'] / $contrib_data['total_count']) * 100), 2) : 0;
      $data['graph_data']['module_compatibility']['inactive_percent'] =
      !empty($contrib_data['total_count']) ? round((($contrib_data['not_active'] / $contrib_data['total_count']) * 100), 2) : 0;
      $data['graph_data']['module_compatibility']['not_compatible'] = $contrib_data['not_higher_version_compatible'];
      $data['graph_data']['module_compatibility']['not_compatible_percent'] =
      round((($contrib_data['not_higher_version_compatible'] / $contrib_data['active']) * 100), 2);
    }
    $contrib_data['impact_on_performance'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
    $contrib_data['migration_complexity'] = $grade;
    $data['graph_data']['module_compatibility']['grade'] = $contrib_data['migration_complexity'];
    if ($contrib_data['migration_complexity'] == '-') {
      $tool_tip_content = t('Not considered as no custom modules present.');
    }
    elseif ($contrib_data['migration_complexity'] == MIGRATION_ADVISOR_LOW_GRADE) {
      $tool_tip_content = t('<strong>Grade @grade_p :</strong>
      Most contrib modules are compatible with higher drupal version.', array(
        '@grade_p' => $contrib_data['migration_complexity'],
      ));
    }
    elseif ($contrib_data['migration_complexity'] == MIGRATION_ADVISOR_MODERATE_GRADE) {
      $tool_tip_content = t('<strong>Grade @grade_p :</strong>
      Considerable contrib modules are compatible with higher drupal version.', array(
        '@grade_p' => $contrib_data['migration_complexity'],
      ));
    }
    elseif ($contrib_data['migration_complexity'] == MIGRATION_ADVISOR_COMPLEX_GRADE) {
      $tool_tip_content = t('<strong>Grade @grade_p :</strong>
      Many contrib modules are not compatible with higher drupal version.', array(
        '@grade_p' => $contrib_data['migration_complexity'],
      ));
    }
    $contrib_data['complexity_desc'] = $tool_tip_content;
  }

  $data['main_table']['data']['contribution'] = $contrib_data;

}

/**
 * Webforms Data.
 */
function migration_advisor_get_webform_count(&$data) {

  $query = db_select('node', 'n')
    ->fields('n', array('status'))
    ->condition('type', 'webform');
  $query->addExpression('count(type)', 'count');
  $query->groupBy('status');
  $result = $query->execute()->fetchAll();

  $webforms = $data['main_table']['data']['webforms'];
  $published = 0;
  $unpublished = 0;
  foreach ($result as $webform_status_count) {
    if ($webform_status_count->status == 1) {
      $published = $webform_status_count->count;
    }
    else {
      $unpublished = $webform_status_count->count;
    }
  }

  $webforms['total_count'] = $published + $unpublished;
  $webforms['active'] = $published;
  $webforms['not_active'] = $unpublished;
  $webforms['not_higher_version_compatible'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $webforms['impact_on_performance'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $webforms['migration_complexity'] = migration_advisor_get_complexity('webforms', $published);
  if ($webforms['migration_complexity'] == '-') {
    $tool_tip_content = t('Not considered as no webform used.');
  }
  elseif ($webforms['migration_complexity'] == MIGRATION_ADVISOR_LOW_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Less complex as no active webforms.', array(
      '@grade_p' => $webforms['migration_complexity'],
    ));
  }
  elseif ($webforms['migration_complexity'] == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Webforms with considerable nodes to migrate.', array(
      '@grade_p' => $webforms['migration_complexity'],
    ));
  }
  elseif ($webforms['migration_complexity'] == MIGRATION_ADVISOR_COMPLEX_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Webforms with considerable nodes to migrate.', array(
      '@grade_p' => $webforms['migration_complexity'],
    ));
  }
  $webforms['complexity_desc'] = $tool_tip_content;
  $data['main_table']['data']['webforms'] = $webforms;

  // Update graph data:
  $data['graph_data']['webforms']['total_count'] = $webforms['total_count'];
  $data['graph_data']['webforms']['active'] = $webforms['active'];
  $data['graph_data']['webforms']['inactive'] = $webforms['not_active'];
  $data['graph_data']['webforms']['active_percent'] =
  !empty($webforms['total_count']) ? round((($webforms['active'] / $webforms['total_count']) * 100), 2) : 0;
  $data['graph_data']['webforms']['inactive_percent'] =
  !empty($webforms['total_count']) ? round((($webforms['not_active'] / $webforms['total_count']) * 100), 2) : 0;
  $data['graph_data']['webforms']['grade'] = $webforms['migration_complexity'];
}

/**
 * Fetch the cound of nodes and update the data array.
 */
function migration_advisor_get_node_count(&$data) {

  $query = db_select('node', 'n')
    ->fields('n', array('status'))
    ->condition('type', 'webform', '<>');
  $query->addExpression('count(type)', 'count');
  $query->groupBy('status');
  $result = $query->execute()->fetchAll();

  $content_type = $data['main_table']['data']['content'];
  $published = 0;
  $unpublished = 0;
  foreach ($result as $node_status_count) {
    if ($node_status_count->status == 1) {
      $published = $node_status_count->count;
    }
    else {
      $unpublished = $node_status_count->count;
    }
  }

  $content_type['total_count'] = $published + $unpublished;
  $content_type['active'] = $published;
  $content_type['not_active'] = $unpublished;
  $content_type['not_higher_version_compatible'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $content_type['impact_on_performance'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $content_type['migration_complexity'] = migration_advisor_get_complexity('content', $published);
  if ($content_type['migration_complexity'] == '-') {
    $tool_tip_content = t('As no contents present to migrate.');
  }
  elseif ($content_type['migration_complexity'] == MIGRATION_ADVISOR_LOW_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    None published contents/nodes to migrate.', array(
      '@grade_p' => $content_type['migration_complexity'],
    ));
  }
  elseif ($content_type['migration_complexity'] == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Considerable amount of content to migrate.', array(
      '@grade_p' => $content_type['migration_complexity'],
    ));
  }
  elseif ($content_type['migration_complexity'] == MIGRATION_ADVISOR_COMPLEX_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Considerable amount of content to migrate.', array(
      '@grade_p' => $content_type['migration_complexity'],
    ));
  }
  $content_type['complexity_desc'] = $tool_tip_content;

  $data['main_table']['data']['content'] = $content_type;

  // Update graph data:
  $data['graph_data']['content']['total_count'] = $content_type['total_count'];
  $data['graph_data']['content']['active'] = $content_type['active'];
  $data['graph_data']['content']['inactive'] = $content_type['not_active'];
  $data['graph_data']['content']['active_percent'] =
  !empty($content_type['total_count']) ? round((($content_type['active'] / $content_type['total_count']) * 100), 2) : 0;
  $data['graph_data']['content']['inactive_percent'] =
  !empty($content_type['total_count']) ? round((($content_type['not_active'] / $content_type['total_count']) * 100), 2) : 0;
  $data['graph_data']['content']['grade'] = $content_type['migration_complexity'];
}

/**
 * Environment Score.
 */
function migration_advisor_get_environment_score(&$data) {
  $environment = $data['main_table']['data']['environment'];
  $environment['total_count'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $environment['active'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $environment['not_active'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $environment['not_higher_version_compatible'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $environment['impact_on_performance'] = MIGRATION_ADVISOR_NOT_APPLICABLE_LABEL;
  $environment['migration_complexity'] = $_GET['radio'];
  if ($environment['migration_complexity'] == '-') {
    $tool_tip_content = t('As you have selected not able to decide the environment.');
  }
  elseif ($environment['migration_complexity'] == MIGRATION_ADVISOR_LOW_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Less complex to migrate if new app to deploy on same server.', array(
      '@grade_p' => $environment['migration_complexity'],
    ));
  }
  elseif ($environment['migration_complexity'] == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    Because if server is different but rest all softwares remain
    same then complexity is moderate.', array(
      '@grade_p' => $environment['migration_complexity'],
    ));
  }
  elseif ($environment['migration_complexity'] == MIGRATION_ADVISOR_COMPLEX_GRADE) {
    $tool_tip_content = t('<strong>Grade @grade_p :</strong>
    More complex if both server and environment is new.', array(
      '@grade_p' => $environment['migration_complexity'],
    ));
  }
  $environment['complexity_desc'] = $tool_tip_content;
  $data['main_table']['data']['environment'] = $environment;
}

/**
 * Fetch compatibility grade.
 */
function migration_advisor_get_complexity_compatibility($not_compatible_percent) {
  if ($not_compatible_percent <= 10) {
    return MIGRATION_ADVISOR_LOW_GRADE;
  }
  elseif ($not_compatible_percent < 30) {
    return MIGRATION_ADVISOR_MODERATE_GRADE;
  }
  elseif ($not_compatible_percent >= 30) {
    return MIGRATION_ADVISOR_COMPLEX_GRADE;
  }
}

/**
 * Associate Task to its  Consider Evaluation bool.
 */
function migration_advisor_get_evaluate_grade_status($task) {
  $options = array(
    'core' => 0,
    'contributed' => 1,
    'custom' => 1,
    'themes' => 1,
    'views' => 1,
    'webforms' => 1,
    'custom_blocks' => 1,
    'content' => 1,
    'menu' => 0,
    'user_roles' => 0,
    'users' => 0,
    'media_files' => 1,
    'environment' => 1,
  );
  return !isset($options[$task]) ? 0 : $options[$task];
}

/**
 * Associate grades with initial value.
 */
function migration_advisor_get_grade_associate_value($grade) {
  $options = array(
    MIGRATION_ADVISOR_LOW_GRADE => 1,
    MIGRATION_ADVISOR_MODERATE_GRADE => 2,
    MIGRATION_ADVISOR_COMPLEX_GRADE => 3,
  );
  return !isset($options[$grade]) ? 0 : $options[$grade];
}

/**
 * Fetch overall grade.
 */
function migration_advisor_get_overall_grade(&$data) {

  $grades_category = array();
  foreach ($data['main_table']['data'] as $data_content) {
    if ($data_content['migration_complexity'] == '-') {
      continue;
    }

    if (!isset($grades_category[$data_content['migration_complexity']])) {
      $grades_category[$data_content['migration_complexity']] = 0;
    }
    $grades_category[$data_content['migration_complexity']]++;

  }
  if (isset($grades_category[0])) {
    unset($grades_category[0]);
  }

  $max_grade_count = 0;
  $max_grade_val = 0;
  $final_grade = '-';
  foreach ($grades_category as $grade_key => $grade_count) {
    if ($grade_count > $max_grade_count) {
      $max_grade_count = $grade_count;
      $max_grade_val = migration_advisor_get_grade_associate_value($grade_key);
      $final_grade = $grade_key;
    }
    elseif ($grade_count == $max_grade_count) {
      $tmp_grade = migration_advisor_get_grade_associate_value($grade_key);
      if ($max_grade_val < $tmp_grade) {
        $max_grade_val = $tmp_grade;
        $final_grade = $grade_key;
      }
    }
  }
  $grade_description = "";
  $grade_effort = "";
  if ($final_grade == MIGRATION_ADVISOR_LOW_GRADE) {
    $grade_description = t("Less manual effort to migrate to higher version.");
    $grade_effort = t("4-5 Working Weeks");
  }
  elseif ($final_grade == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $grade_description = t("Considerable amount of manual work to migrate to higher version.");
    $grade_effort = t("5-6 Working Weeks");
  }
  elseif ($final_grade == MIGRATION_ADVISOR_COMPLEX_GRADE) {
    $grade_description = t("More manual effort to migrate to higher version.");
    $grade_effort = t("6-7 Working Weeks");
  }
  $data['migration_grade']['grade'] = $final_grade;
  $data['migration_grade']['description'] = $grade_description;
  $data['migration_grade']['effort'] = $grade_effort;

  $media_grade = $data['main_table']['data']['media_files']['migration_complexity'];
  if ($media_grade == MIGRATION_ADVISOR_LOW_GRADE) {
    $data['migration_grade']['media_description'] =
    t('Since majority of the files are within the Less Than 150 Kb threshold,
    the process of migrating media files and impact on performance of website is significantly less.');
  }
  elseif ($media_grade == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $data['migration_grade']['media_description'] =
    t('Since majority of the files are within the Between 150 Kb and 500 Kb threshold,
    the process of migrating media files and impact on performance of website is average.');
  }
  elseif ($media_grade == MIGRATION_ADVISOR_MODERATE_GRADE) {
    $data['migration_grade']['media_description'] =
    t('Since majority of the files are within the Greater Than 500KB threshold,
    the process of migrating media files and impact on performance of website is complex.');
  }
  else {
    $data['migration_grade']['media_description'] = '-';
  }

}

/**
 * Update module grade count.
 */
function migration_advisor_update_grade_count(&$data) {

  $grade_count = array();
  foreach ($data['main_table']['data'] as $module_category) {
    if ($module_category['migration_complexity'] == '-') {
      continue;
    }
    if (!isset($grade_count[$module_category['migration_complexity']])) {
      $grade_count[$module_category['migration_complexity']] = 0;
    }
    $grade_count[$module_category['migration_complexity']]++;
  }
  $data['grade_counts']['total_modules'] = count($data['main_table']['data']);
  $data['grade_counts']['high_effort'] =
  isset($grade_count[MIGRATION_ADVISOR_COMPLEX_GRADE]) ? $grade_count[MIGRATION_ADVISOR_COMPLEX_GRADE] : 0;
  $data['grade_counts']['medium_effort'] =
  isset($grade_count[MIGRATION_ADVISOR_MODERATE_GRADE]) ? $grade_count[MIGRATION_ADVISOR_MODERATE_GRADE] : 0;
  $data['grade_counts']['low_effort'] =
  isset($grade_count[MIGRATION_ADVISOR_LOW_GRADE]) ? $grade_count[MIGRATION_ADVISOR_LOW_GRADE] : 0;
}
