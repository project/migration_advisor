README.txt for Migration Advisor module
---------------------------
  
CONTENTS OF THIS FILE
---------------------
  
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
  
INTRODUCTION
------------

Migration Advisor module provides an assessment of the
migration complexity and expected time-frame to upgrade from
your existing Drupal 7.x version to later versions like
Drupal 8.x or 9.x. Based on the assessment, a report is
automatically generated that shows a list of modules to be
migrated and their associated complexity in terms of manual
effort needed.


REQUIREMENTS
------------

This module is dependent on libraries module(https://www.drupal.org/project/libraries). 
We need to have it enabled before installing the module.


INSTALLATION
------------

 - Install the User Instance module as you would normally install a 
 contributed Drupal module. Visit https://www.drupal.org/node/1897420
 for further information.


CONFIGURATION
-------------

1. Place this module in your /modules directory and enable it.
2. Go to /admin/reports and click on Migration Advisor to provide
initial input for report.
3. Complete initial assessment to go main report page. If no errors
in initial report then more accurate the report will be.
4. If in initial assessment it shows failed continously then try again
after 5 mins or so as caching comes into picture.
5. Final report will have both graphical and table representation of data.


MAINTAINERS
-----------

 - Naveen Kumar N (naveen-k) https://www.drupal.org/u/naveen-k
 - Shrenik https://www.drupal.org/u/shrenikm

 - Supporting organizations
   Moonraft Innovation Labs
