<?php

/**
 * @file
 * Theme implementation to show reports of migration.
 *
 * Available variables:
 * - $data: An array of sections to be displayed.
 * Keyed by section name with the associative array of data as value.
 *
 * @ingroup themeable
 */
?>
<?php
$path = drupal_get_path('module', 'migration_advisor');
drupal_add_css($path . '/css/migration_style.css',
array('group' => CSS_DEFAULT, 'every_page' => TRUE)
);
drupal_add_js($path . '/js/Chart.min.js');
drupal_add_js($path . '/js/chartjs-plugin-datalabels.min.js');
$chart_data = array(
  'all_modules_data' => $data['graph_data'],
);
drupal_add_js(array('chart_data' => $chart_data), 'setting');
drupal_add_js($path . '/js/migration_chart.js');
?>
<div class="advisor-main-container">
<div class="header">
  <div><h1><?php print t('Your report is ready!'); ?></h1></div>
  <div class="print"><a href="javascript:void();" id="printButton" onclick={window.print()}><?php print t('Print Report'); ?></a></div>
</div>
<div class="advisor-modal-content" id="print-me">
    <?php foreach($data as $section => $section_data):
    if($section === "configuration"):?>
    <div class="advisor-table-container advisor-config-list">
      <?php
        if ($_GET['radio'] == "A") {
          $radio_text = MIGRATION_ADVISOR_SAME_SERVER_LABEL;
        } elseif ($_GET['radio'] == "B") {
          $radio_text = MIGRATION_ADVISOR_DIFF_SERVER_LABEL;
        } elseif ($_GET['radio'] == "C") {
          $radio_text = MIGRATION_ADVISOR_DIFF_ENVIRON_LABEL;
        } else {
          $radio_text = MIGRATION_ADVISOR_EARLY_LABEL;
        }
      ?>      
        <div class="config-text">          
          <?php print t('Migrating from Drupal <strong>:drupal_version</strong> to <strong>:migration_version</strong> on <strong>:environment</strong>',
          array(
            ':drupal_version' => $section_data['data']['drupal_version'],
            ':migration_version' => variable_get('migration_advisor_migration_version', MIGRATION_ADVISOR_CORE_VERSION),
            ':environment' => $radio_text,
          )
          ); ?>
        </div>
        <span class="config-title">
          <?php print t('View Full Configuration List'); ?>
          <span class='m-advisor-tooltip return-icon'>
            <span class='ma-tooltiptext'>
              <table>
            <?php foreach($section_data['row_names'] as $key => $value){
                print "<tr><td>" . $value . " - <strong>" . $section_data['data'][$key] . "</strong></td>";
                print "</tr>";
            }
                 ?>
        </table>
            </span>
          </span>
        </span>        
    </div>
    <?php endif;
    if($section == 'migration_grade'): ?>
    <div class="advisor-table-container advisor-grade-container">
      <div class="grade-container-left">      
        <h1><?php print t('Migration Grade'); ?></h1>       
        <span class='m-advisor-tooltip return-icon'>
            <span class='ma-tooltiptext'>
              <?php foreach ($section_data['grades_table'] as $grade_content_key => $grade_content_value) : ?>
                <?php if ($grade_content_key === "table_value") : ?>
                  <div class="advisor-info-container">
                    <?php foreach ($grade_content_value as $grade_key => $grade_value) : ?>            
                      <?php print "<div class='grade-" . $grade_value[0] . "'>" .
                      t('<strong>Grade @grade</strong>&nbsp- @grade_d </div>',
                      array(
                        '@grade' => $grade_value[0],
                        '@grade_d' => $grade_value[1],
                      )
                      ); ?>
                    <?php endforeach; ?>
                  </div>
                <?php endif; ?>
              <?php endforeach; ?>      
            </span>
          </span>
        <div class="grade-content">
          <h2><?php print t('Grade'); ?></h2>
          <div class="grade <?php print 'advisor-grade-container-' . $section_data['grade']; ?>">
            <span><?php print $section_data['grade']; ?></span>            
          </div>
          <div class="grade-desc"><?php print $section_data['description']; ?></div>
          <div class="effort"><span><?php print t('Approximate Effort'); ?>:</span><br/><strong><?php print $section_data['effort']; ?></strong></div>
        </div>
      </div>        
      <div class="grade-container-right">
        <h2><?php print t('Why the grade?'); ?></h2>
        <div class="grade-content">
          <div>
            <div class="img-container">
              <img src="<?php print base_path() . $path . "/img/modules.png"; ?>" />
            </div>
            <div class="content">
              <div><?php print $data['grade_counts']['total_modules'];?></div>
              <div><?php print t('Total Modules Checked'); ?></div>
            </div>
          </div>
          <div>
            <div class="img-container">
              <img src="<?php print base_path() . $path . "/img/high.png"; ?>" />
            </div>
            <div class="content">
              <div><?php print $data['grade_counts']['high_effort'];?></div>
              <div><?php print t('High Migration Efforts'); ?></div>
            </div>
          </div>
          <div>
            <div class="img-container">
              <img src="<?php print base_path() . $path . "/img/medium.png"; ?>" />
            </div>
            <div class="content">
              <div><?php print $data['grade_counts']['medium_effort'];?></div>
              <div><?php print t('Medium Migration Efforts'); ?></div>
            </div>
          </div>
          <div>
            <div class="img-container">
              <img src="<?php print base_path() . $path . "/img/low.png"; ?>">
            </div>
            <div class="content">
              <div><?php print $data['grade_counts']['low_effort'];?></div>
              <div><?php print t('Low Migration Efforts'); ?></div>
            </div>
          </div>
          <div class="note">
            <div class="img-container">
              <img src="<?php print base_path() . $path . "/img/info.png"; ?>">
            </div>
            <div class="content">
              <strong><?php print t('Important Note:'); ?></strong><br/>
              <?php print t('This report is generated based on the assessment run on your site. It is only to identify the technical complexity. Grade will be one of many factors considered to estimate the project.'); ?>
            </div>
          </div>  
        </div>       
      </div>      
    </div>
    <?php endif; ?>         
    <?php if($section === "graph_data"){?>    
    <div class="compatibility-graph-container" >
    <h1><?php print t('Where are the issues?'); ?></h1>
    <div class="ma-subtitle-container">
      <div class="ma-sub-title"><?php print t('Type-wise Breakdown of Modules'); ?></div>
      <div>
        <span class="ma-chart-label"><?php print t('Chart'); ?></span>
        <label class="ma-switch">
          <input type="checkbox">
          <span class="ma-slider round"></span>
        </label>
        <span><?php print t('Table'); ?></span>
      </div>
    </div>
    <div class="adv-chart-maincontainer2">
        <div class="chart-container chart-media">
            <canvas id='ma_overallchart' style="height:40vh; width:100vw"></canvas>
        </div>
    </div>
        
    </div>
    <?php } ?>
    <?php
    if($section === "main_table"){?>
    <div class="advisor-table-container advisor-table-container-main-table">        
        <table class="migration-adv-table migration-main-table">
            <?php
                foreach($section_data as $section_data_key => $section_data_content):
                    if($section_data_key === 'column_names'){
                        print "<thead><tr>";
                            foreach ($section_data_content as $main_table_columns_key => $main_table_columns){
                                if ($main_table_columns_key == 'complexity_desc') {
                                    continue;
                                }
                                print "<th>" . $main_table_columns . "</th>";
                            }
                        print "</tr></thead>";
                    }
                    elseif($section_data_key === 'row_names'){
                        foreach($section_data_content as $main_table_key => $main_table_rows){
                          if ($main_table_key == 'media_files') {
                            continue;
                          }
                            print "<tr><td><strong>" . $main_table_rows . "</strong></td>";
                            foreach($data['main_table']['data'][$main_table_key] as $maintable_data_key => $maintable_data){
                                if ($maintable_data_key == 'complexity_desc') {
                                    continue;
                                }
                                if ($maintable_data_key == 'migration_complexity') {
                                    print "<td>" . "<span class='ma-last-elem" . "'><span class='m-complexity advisor-grade-container-" . $maintable_data . "'>" . $maintable_data .
                                    "</span></span><span class='m-advisor-tooltip return-icon'><span class='ma-tooltiptext'>" .
                                    $data['main_table']['data'][$main_table_key]['complexity_desc'] .
                                      '</span></span>' . "</td>";
                                }
                                else {
                                    print "<td>" . $maintable_data . "</td>";
                                }
                            }
                            print "</tr>";
                        }
                    }
                endforeach
                ?>
        </table>
    </div>
    <?php } ?>
    <?php if ($section === 'media_files_data') : ?>
      <table class="migration-adv-table" id="graph">
        <?php foreach (array_reverse($section_data) as $media_key => $media_content) :
          if ($media_key === 'media_files') :
            print "<tr><td><strong>" . t('Media files') . "</strong></td>";
            foreach ($media_content['data'] as $media_files_key => $media_files_content) :
              if ($media_files_key == 'complexity_desc') :
                continue;
              endif;
              if ($media_files_key === "migration_complexity") :
                print "<td>" . "<span class='ma-last-elem'><span class='m-complexity advisor-grade-container-" . $media_files_content . "'>" . $media_files_content .
                  "</span></span><span class='m-advisor-tooltip return-icon'><span class='ma-tooltiptext'>" .
                  $media_content['data']['complexity_desc'] .
                  '</span></span>' . "</td>";
              else:
                print "<td>" . $media_files_content . "</td>";
              endif;
            endforeach;
            print "</tr><tr>";
          else :
            if($media_key === 'file_distribution') :
              print "<td colspan ='2' class='" . $media_key . "'>";
            else :
               print "<td>";
            endif;
            foreach (array_reverse($media_content) as $m_key => $m_value) :
              if($m_key == 'value') :
                $attributes = "id='" . $media_key . "' data-percent='" . $m_value . "' data-size='80' data-line=8 data-rotate=0";
                print "<div " . $attributes . "></div>";
              else :
                print "<div>" . $m_value . "</div>";
              endif;
            endforeach;
            print "</td>";
          endif;
        endforeach;
        ?>
      <td colspan='2'><?php print $data['migration_grade']['media_description'];?></td>
      </tr></table>    
    <?php endif; ?>    
    <?php endforeach; ?>  
    <!-- Need an elaborate estimation -->
    <div class="advisor-table-container adv-button-container">
      <h1><?php print t('Need an elaborate estimation?'); ?></h1>
      <div><?php print t('If you have questions or want a more elaborate estimation, don’t hesitate to reach out to us.'); ?></div>
      <div class="button">        
        <?php print l(t('Send an email'), 'mailto:promit@moonraft.com?subject=Drupal%20Migration%20discussion%20inquiry&body=Hi,
        %0D%0DI%20have%20run%20the%20migration%20assessment%20tool%20and%20would%20be%20interested%20in%20discussing%20further%20about%20our%20Drupal%20migration%20plans.
        %0D%0DPlease%20get%20in%20touch%20with%20me%20at%20your%20earliest%20convenience%20on%20the%20contact%20details%20below:
        %0D%0DFull%20Name:
        %0DBusiness%20Email%20Id:
        %0DPreferred%20time%20slot%20for%20a%20conference%20call%20(Date,Time,Timezone):
        %0D%0DThanks,', array('attributes' => array('class' => array('rectangle')))); ?>
      </div>
    </div>  
    <div class="advisor-table-container footer">
      <img src="<?php print base_path() . $path . "/img/mr-ust-2.svg"; ?>" />
    </div>
</div>
</div>
