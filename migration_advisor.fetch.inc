<?php

/**
 * @file
 * Retrieve available update data.
 */

module_load_include('inc', 'update', 'update.fetch');

/**
 * Manually checking of updates.
 */
function migration_advisor_manual_status() {
  _migration_advisor_refresh();
  $batch = array(
    'operations' => array(
      array('migration_advisor_fetch_data_batch', array()),
    ),
    'finished' => 'migration_advisor_fetch_data_finished',
    'title' => t('Checking available upgrade version data'),
    'progress_message' => t('Trying to check available upgrade data ...'),
    'error_message' => t('Error checking available upgrade data.'),
    'file' => drupal_get_path('module', 'migration_advisor') . '/migration_advisor.fetch.inc',
  );
  batch_set($batch);
  batch_process('admin/reports/updates/upgradeadvise');
}

/**
 * Fetch Batch: Processes a step in batch for fetching available update data.
 *
 * @param array $context
 *   Reference to an array used for Batch API storage.
 */
function migration_advisor_fetch_data_batch(array &$context) {
  $queue = DrupalQueue::get('migration_advisor_fetch_tasks');
  if (empty($context['sandbox']['max'])) {
    $context['finished'] = 0;
    $context['sandbox']['max'] = $queue->numberOfItems();
    $context['sandbox']['progress'] = 0;
    $context['message'] = t('Checking available upgrade data ...');
    $context['results']['updated'] = 0;
    $context['results']['failures'] = 0;
    $context['results']['processed'] = 0;
  }

  // Grab another item from the fetch queue.
  for ($i = 0; $i < 5; $i++) {
    if ($item = $queue->claimItem()) {
      if (_migration_advisor_process_fetch_task($item->data)) {
        $context['results']['updated']++;
        $context['message'] = t('Checked available upgrade data for %title.', array('%title' => $item->data['info']['name']));
      }
      else {
        $context['message'] = t('Failed to check available upgrade data for %title.', array('%title' => $item->data['info']['name']));
        $context['results']['failures']++;
      }
      $context['sandbox']['progress']++;
      $context['results']['processed']++;
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      $queue->deleteItem($item);
    }
    else {
      $context['finished'] = 1;
      return;
    }
  }
}

/**
 * Drain the queue of tasks.
 */
function _migration_advisor_retrieve_data() {
  $queue = DrupalQueue::get('migration_advisor_fetch_tasks');
  $end = time() + variable_get('update_max_fetch_time', UPDATE_MAX_FETCH_TIME);
  while (time() < $end && ($item = $queue->claimItem())) {
    _migration_advisor_process_fetch_task($item->data);
    $queue->deleteItem($item);
  }
}

/**
 * Fetch available update data for a single project.
 *
 * @param array $project
 *   Associative array of information about the project to fetch data for.
 *
 * @return bool
 *   TRUE if we fetched parsable XML, otherwise FALSE.
 */
function _migration_advisor_process_fetch_task(array $project) {
  $fail = &drupal_static(__FUNCTION__, array());
  $now = time();
  if (empty($fail)) {
    if (($cache = _update_cache_get('migration_advisor_fetch_failures')) && ($cache->expire > $now)) {
      $fail = $cache->data;
    }
  }

  $max_fetch_attempts = variable_get('update_max_fetch_attempts', UPDATE_MAX_FETCH_ATTEMPTS);

  $success = FALSE;
  $available = array();
  $version = variable_get('migration_advisor_core_version', MIGRATION_ADVISOR_CORE_VERSION);
  $url = _migration_advisor_build_fetch_url($project, $version);
  $project_name = $project['name'];

  if (empty($fail[$url]) || $fail[$url] < $max_fetch_attempts) {
    $xml = drupal_http_request($url);
    if (!isset($xml->error) && isset($xml->data)) {
      $data = $xml->data;
    }
  }
  if (!empty($data)) {
    $available = update_parse_xml($data);
    $success = TRUE;
  }
  else {
    $available['project_status'] = 'not-fetched';
    if (empty($fail[$url])) {
      $fail[$url] = 1;
    }
    else {
      $fail[$url]++;
    }
  }

  $frequency = variable_get('update_check_frequency', 1);
  $cid = 'migration_advisor_available_releases::' . $project_name;
  _update_cache_set($cid, $available, $now + (60 * 60 * 24 * $frequency));

  // Stash the $fail data back in the DB for the next 1 minute.
  _update_cache_set('migration_advisor_fetch_failures', $fail, $now + (60 * 1));

  variable_set('migration_advisor_last_check', $now);

  // Clear out the record in {cache_update} for this task
  // so we're willing to fetch again.
  _update_cache_clear('migration_advisor_fetch_task::' . $project_name);

  return $success;
}

/**
 * Implements callback_batch_finished().
 *
 * Performs actions when all fetch tasks have been completed.
 *
 * @param bool $success
 *   TRUE if the batch operation was successful; FALSE if there were errors.
 * @param array $results
 *   An associative array of results from the batch operation, including the key
 *   'updated' which holds the total number of projects we fetched available
 *   update data for.
 */
function migration_advisor_fetch_data_finished(bool $success, array $results) {
  if ($success) {
    if (!empty($results)) {
      if (!empty($results['updated'])) {
        drupal_set_message(format_plural($results['updated'], 'Checked available updates data for one project.', 'Checked available updates data for @count projects.'));
      }
      if (!empty($results['failures'])) {
        variable_set('migration_advisor_fetch_failed_flag', 1);
        drupal_set_message(format_plural($results['failures'], 'Failed to get available updates data for one project.', 'Failed to get available updates data for @count projects.'), 'error');
      }
      else {
        variable_set('migration_advisor_fetch_failed_flag', 0);
      }
    }
  }
  else {
    drupal_set_message(t('An error occurred trying to get available update data.'), 'error');
    variable_set('migration_advisor_fetch_failed_flag', 0);
  }
}

/**
 * Clears out all the cached available update data and initiates re-fetching.
 */
function _migration_advisor_refresh() {
  module_load_include('inc', 'migration_advisor', 'migration_advisor.compare');

  // Since its a refresh, we want to clear our cache of both the projects.
  _update_cache_clear('migration_advisor_project_projects');
  _update_cache_clear('migration_advisor_project_data');

  $projects = update_get_projects();

  // Obsolete projects.
  // Add replacement modules to the list of projects to get XML data for.
  foreach ($projects as $key => $project) {
    if (migration_advisor_obsolete($projects, $key)) {
      // Add the project that makes this one obsolete to the list of those to
      // grab information about.
      foreach ($projects[$key]['replaced_by'] as $replacement) {
        $projects[$replacement['name']] = array(
          'name' => $replacement['name'],
        );
      }
    }
  }

  _update_cache_clear('migration_advisor_available_releases::', TRUE);

  foreach ($projects as $key => $project) {
    migration_advisor_create_retrieve_data_task($project);
  }
}

/**
 * Adds a task to the queue for fetching release history data.
 *
 * @param array $project
 *   Associative array of information about a project.
 */
function _migration_advisor_create_retrieve_data_task(array $project) {
  $fetch_tasks = &drupal_static(__FUNCTION__, array());
  if (empty($fetch_tasks)) {
    $fetch_tasks = _update_get_cache_multiple('migration_advisor_fetch_task');
  }
  $cid = 'migration_advisor_fetch_task::' . $project['name'];
  if (empty($fetch_tasks[$cid])) {
    $queue = DrupalQueue::get('migration_advisor_fetch_tasks');
    $queue->createItem($project);
    try {
      db_insert('cache_update')
        ->fields(array(
          'cid' => $cid,
          'created' => REQUEST_TIME,
        ))
        ->execute();
    }
    catch (Exception $e) {
      // The exception can be ignored safely.
    }
    $fetch_tasks[$cid] = REQUEST_TIME;
  }
}

/**
 * Generates the URL to fetch information about project updates.
 *
 * @param array $project
 *   The array of project information.
 * @param string $version
 *   The target version of Drupal core you wish to query.
 *
 * @return string
 *   The URL for fetching information about updates to the specified project.
 */
function _migration_advisor_build_fetch_url(array $project, string $version) {
  $name = $project['name'];
  $url = _update_get_fetch_url_base($project);
  $url .= '/' . $name . '/' . $version;

  return $url;
}
