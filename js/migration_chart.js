/**
 * @file
 */
(function ($) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {

      //Media Files - Circular graph
      var arr = ['less_than_150kb', 'between_150kb_and_500kb', 'greater_than_500kb'];
      var color = ['#417505', '#ff7940', '#d0021b'];
      for (var i = 0; i < arr.length; i++) {
        var e1 = document.getElementById(arr[i]);
        var options = {
          percent:  e1.getAttribute('data-percent') || 25,
          size: e1.getAttribute('data-size') || 220,
          lineWidth: e1.getAttribute('data-line') || 15,
          rotate: e1.getAttribute('data-rotate') || 0
        }

        var canvas = document.createElement('canvas');
        var span = document.createElement('span');
        span.classList.add("data-percent");
        span.textContent = options.percent + '%';

        if (typeof(G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }

        var mediactx = canvas.getContext('2d');
        canvas.width = canvas.height = options.size;

        e1.appendChild(span);
        e1.appendChild(canvas);

        mediactx.translate(options.size / 2, options.size / 2); // change center
        mediactx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

        //imd = mediactx.getImageData(0, 0, 240, 240);
        var radius = (options.size - options.lineWidth) / 2;

        var drawCircle = function (color, lineWidth, percent) {
          percent = Math.min(Math.max(0, percent || 1), 1);
          mediactx.beginPath();
          mediactx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
          mediactx.strokeStyle = color;
          mediactx.lineCap = 'round'; // butt, round or square
          mediactx.lineWidth = lineWidth
          mediactx.stroke();
        };
        drawCircle('#efefef', 5, 100 / 100);
        drawCircle(color[i], options.lineWidth, options.percent / 100);
      }

      var countChecked = function () {
        if (typeof($( "input:checked + .ma-slider" ).val()) === "undefined") {
          $('.adv-chart-maincontainer2').css({"height": "auto", "opacity": 1, "overflow": "visible"});
          $('.advisor-table-container-main-table').css({"height": "0", "opacity": 0,
          "transition": "opacity 1s ease-out", "overflow": "hidden"});
        }
        else {
          $('.adv-chart-maincontainer2').css({"height": "0.1px", "opacity": 0,
          "transition": "opacity 1s ease-out", "overflow": "visible"});
          $('.advisor-table-container-main-table').css({"height": "auto", "opacity": 1, "overflow": "hidden"});
        }
      };

      countChecked();

      $( "input[type=checkbox]" ).bind( "click", countChecked );

      //Stack chart;

      // Fetch dat from drupal.
      var allModuleData = Drupal.settings.chart_data.all_modules_data;
      delete allModuleData['media_data'];
      var moduleLabels = [];
      const moduleGrades = [];
      const moduleActiveInactiveCounts = [];
      const activeData = [];
      const inactiveData = [];
      const contribData = [];
      var contributionLabelName = '';
      // Create data structure for appending into data.
      Object.keys(allModuleData).forEach(key => {
        moduleLabels.push(allModuleData[key].label);
        moduleGrades[allModuleData[key].label] = allModuleData[key].grade;
        activeData.push(allModuleData[key].active_percent);
        inactiveData.push(allModuleData[key].inactive_percent);
        if(key == 'module_compatibility') {
          contribData.push(allModuleData[key].not_compatible_percent);
          moduleActiveInactiveCounts[allModuleData[key].label] = {"active": allModuleData[key].active,
          "inactive": allModuleData[key].inactive, "total": allModuleData[key].total_count,
         "incompatible": allModuleData[key].not_compatible};
          contributionLabelName = allModuleData[key].label;
        }
        else {
          contribData.push(0);
          moduleActiveInactiveCounts[allModuleData[key].label] = {"active": allModuleData[key].active,
          "inactive": allModuleData[key].inactive, "total": allModuleData[key].total_count,
         "incompatible": 0};
        }
      });

      // Label formatter function to display percentage.
      const ctx = document.getElementById("ma_overallchart").getContext("2d");
      const formatter = (value, ctx) => {
        const otherDatasetIndex = ctx.datasetIndex === 0 ? 1 : 0;
        const total =
          ctx.chart.data.datasets[otherDatasetIndex].data[ctx.dataIndex] + value;
        if (value == 0) {
          return '';
        }

        return `${(value / total * 100).toFixed(0)} % `;
      };

      const data = [{
        label: "Inactive",
        backgroundColor: "#9b9b9b",
        data: inactiveData,
        datalabels: {
          color: "#white",
          formatter: formatter
        }
      },
      {
        label: "Active",
        backgroundColor: "#417505",
        data: activeData,
        datalabels: {
          color: "#white",
          formatter: formatter
        }
      },
      {
        label: "Incompatible",
        backgroundColor: "#ff324b",
        data: contribData,
        datalabels: {
          color: "#white",
          display: true,
          formatter: formatter
        }
      }
      ];

      const chartOptions = {
        layout: {
          padding: {
            left: 24,
            right: 24,
            top: 24,
            bottom: 24
          }
        },
        maintainAspectRatio: false,
        spanGaps: false,
        responsive: true,
        legend: {
          display: false,
          position: "bottom",
          labels: {
            fontColor: "#black",
            boxWidth: 14
          }
        },
        tooltips: {
          mode: "label",
          callbacks: {
            label: function (tooltipItem, data) {
              var type = data.datasets[tooltipItem.datasetIndex].label;
              //Fetch count for corresponding modules.
              var value = 0;
              if (type == 'Inactive') {
                value = moduleActiveInactiveCounts[tooltipItem.xLabel].inactive;
              }
              else if (type == 'Active') {
                value = moduleActiveInactiveCounts[tooltipItem.xLabel].active;
              }
              else {
                value = moduleActiveInactiveCounts[tooltipItem.xLabel].incompatible;
              }

              let total = 0;
              total = moduleActiveInactiveCounts[tooltipItem.xLabel].total;
              // Ignore Showing Incompatible for Other x-axis.
              if (tooltipItem.xLabel != contributionLabelName
                && (type == 'Incompatible')) {
                return "Total Count : " + total;
              }

              if (tooltipItem.datasetIndex !== data.datasets.length - 1) {
                return (
                  type + " : " + value
                );
              } else {
                //Alter tooltip for Incompatible modules.
                if (tooltipItem.xLabel == contributionLabelName) {
                  return [
                    type + " Active Modules : " +
                    value + " of " + moduleActiveInactiveCounts[tooltipItem.xLabel].active,
                    "Total Modules : " + total
                  ];
                }
                else {
                  data.datasets[2].backgroundColor = "#fff";
                  return [
                    type +
                    " : " +
                    value,
                    "Total Modules : " + total
                  ];
                }
              }
            },
            title:  function (tooltipItem, data) {
              return data.labels[tooltipItem[0].index] + " Counts";
            }
          }
        },
        plugins: {
          // Change options for ALL labels of THIS CHART
          datalabels: {
            color: "#white",
            align: "center"
          }
        },
        scales: {
          xAxes: [{
            stacked: true,
            ticks: {
              fontSize: 16,
              fontColor: "#00000",
            },
            scaleLabel: {
              display: true,
              labelString: "Type Of Modules",
              fontColor: '#006dbe',
              fontSize: 16,
            },
            gridLines: {
              display: false
            },
            barThickness: 50,
            gridLines: {
              drawOnChartArea: false
            }
          },
          {
            type: 'category',
            offset: true,
            position: 'top',
            ticks: {
              callback: function (value, index, values) {
                return "Grade - " + moduleGrades[value];
              },
              fontStyle: "bold",
              fontSize: 16,
            },
            gridLines: {
              display: false,
              drawOnChartArea: false
            }, scaleLabel: {
              display: false,
            }
          }
          ],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: "Percentage",
              fontColor: '#006dbe',
              fontSize: 16,
            },
            stacked: true,
            gridLines: {
              drawOnChartArea: false
            }
          }]
        }
      };

      var chartData = new Chart(ctx, {
        type: "bar",
        data: {
          labels: moduleLabels,
          datasets: data
        },
        options: chartOptions
      });

    },
  };
})(jQuery);
