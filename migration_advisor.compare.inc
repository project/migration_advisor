<?php

/**
 * @file
 * Code required only when comparing available updates to existing data.
 */

module_load_include('inc', 'update', 'update.compare');

/**
 * Calculates the current update status of all projects on the site.
 *
 * @param array $available
 *   Data about available project releases.
 *
 * @return array
 *   An array of installed projects with current update status information.
 *
 * @see migration_advisor_get_available()
 * @see update_get_projects()
 * @see update_process_project_info()
 * @see update_project_cache()
 */
function migration_advisor_calculate_project_data(array $available) {
  // Retrieve the data from cache if available.
  $projects = _update_cache_get('migration_advisor_project_data');

  if (!empty($projects)) {
    return $projects->data;
  }
  $projects = update_get_projects();

  // Handle obsolete projects.
  foreach ($projects as $key => $project) {
    if (migration_advisor_obsolete($projects, $key)) {
      foreach ($projects[$key]['replaced_by'] as $replacement) {
        $projects[$replacement['name']] = $available[$replacement['name']];
        $projects[$replacement['name']]['info'] = array();
      }
    }
  }

  update_process_project_info($projects);
  foreach ($projects as $project => $project_info) {
    if (isset($available[$project])) {
      migration_advisor_calculate_project_update_status($project, $projects[$project], $available[$project]);
    }
    // Check if the project is obsolete.
    elseif (migration_advisor_obsolete($projects, $project)) {
      $projects[$project]['status'] = MIGRATION_ADVISOR_OBSOLETE;
      $projects[$project]['reason'] = t('Made obsolete by');
    }
    else {
      $projects[$project]['status'] = UPDATE_UNKNOWN;
      $projects[$project]['reason'] = t('No available releases found');
    }
  }
  // Give other modules a chance to alter the status (for example, to allow a
  // contrib module to provide fine-grained settings to ignore specific
  // projects or releases).
  drupal_alter('update_status', $projects);

  // Same for us, afterwards.
  drupal_alter('migration_advisor', $projects);

  // Cache the site's update status for at most 1 hour.
  _update_cache_set('migration_advisor_project_data', $projects, REQUEST_TIME + 3600);
  return $projects;
}

/**
 * Calculates the current update status of a specific project.
 *
 * @param string $project
 *   An array containing information about a specific project.
 * @param array $project_data
 *   An array containing information about a specific project.
 * @param array $available
 *   Data about available project releases of a specific project.
 */
function migration_advisor_calculate_project_update_status(string $project, array &$project_data, array $available) {
  foreach (array('title', 'link') as $attribute) {
    if (!isset($project_data[$attribute]) && isset($available[$attribute])) {
      $project_data[$attribute] = $available[$attribute];
    }
  }

  // If the project status is marked as something bad, there's nothing else
  // to consider.
  if (isset($available['project_status'])) {
    switch ($available['project_status']) {
      case 'insecure':
        $project_data['status'] = UPDATE_NOT_SECURE;
        if (empty($project_data['extra'])) {
          $project_data['extra'] = array();
        }
        $project_data['extra'][] = array(
          'class' => array('project-not-secure'),
          'label' => t('Project not secure'),
          'data' => t('This project has been labeled insecure by the Drupal security team, and is no longer available for download. Immediately disabling everything included by this project is strongly recommended!'),
        );
        break;

      // Maintainers are doing lots of nightmares with in development
      // releases, so we have to take unpublished, revoked, and unsupported
      // into account.
      case 'unpublished':
      case 'revoked':
      case 'unsupported':
        break;

      case 'not-fetched':
        $project_data['status'] = UPDATE_NOT_FETCHED;
        $project_data['reason'] = t('Failed to get available update data.');
        break;

      default:
        // Assume anything else (e.g. 'published') is valid and we should
        // perform the rest of the logic in this function.
        break;
    }
  }

  if (!empty($project_data['status'])) {
    $project_data['project_status'] = $available['project_status'];
    return;
  }

  // Figure out the target major version.
  $existing_major = $project_data['existing_major'];
  $supported_majors = array();
  if (isset($available['supported_majors'])) {
    $supported_majors = explode(',', $available['supported_majors']);
  }
  elseif (isset($available['default_major'])) {
    $supported_majors[] = $available['default_major'];
  }

  if (in_array($existing_major, $supported_majors)) {
    // Still supported, stay at the current major version.
    $target_major = $existing_major;
  }
  elseif (isset($available['recommended_major'])) {
    // Since 'recommended_major' is defined, we know this is the new XML
    // format. Therefore, we know the current release is unsupported since
    // its major version was not in the 'supported_majors' list. We should
    // find the best release from the recommended major version.
    $target_major = $available['recommended_major'];
    // Projects may port from 6.x-1.x to 7.x-2.x to change their APIs.
    // $project_data['status'] = UPDATE_NOT_SUPPORTED;.
  }
  elseif (isset($available['default_major'])) {
    // Older release history XML file without recommended, so recommend
    // the currently defined "default_major" version.
    $target_major = $available['default_major'];
  }
  else {
    // Malformed XML file? Stick with the current version.
    $target_major = $existing_major;
  }

  // Some projects are renumbering to 1.x with each new core version.
  // $target_major = max($existing_major, $target_major);.
  $release_patch_changed = '';
  $patch = '';

  // If the project is marked as UPDATE_FETCH_PENDING, it means that the
  // data we currently have (if any) is stale, and we've got a task queued
  // up to (re)fetch the data. In that case, we mark it as such, merge in
  // whatever data we have (e.g. project title and link), and move on.
  if (!empty($available['fetch_status']) && $available['fetch_status'] == UPDATE_FETCH_PENDING) {
    $project_data['status'] = UPDATE_FETCH_PENDING;
    $project_data['reason'] = t('No available update data');
    $project_data['fetch_status'] = $available['fetch_status'];
  }

  if (empty($available['releases'])) {
    $available['releases'] = array();
  }

  foreach ($available['releases'] as $version => $release) {

    // insecure, unpublished, revoked, unsupported have no meaning.
    // See if this is a higher major version than our target and yet still
    // supported. If so, record it as an "Also available" release.
    // Note: some projects have a HEAD release from CVS days, which could
    // be one of those being compared. They would not have version_major
    // set, so we must call isset first.
    if (isset($release['version_major']) && $release['version_major'] > $target_major) {
      if (in_array($release['version_major'], $supported_majors)) {
        if (!isset($project_data['also'])) {
          $project_data['also'] = array();
        }
        if (!isset($project_data['also'][$release['version_major']])) {
          $project_data['also'][$release['version_major']] = $version;
          $project_data['releases'][$version] = $release;
        }
      }

      // Some projects are renumbering to 1.x with each new core version.
      // continue;.
    }

    // Look for the 'latest version' if we haven't found it yet. Latest is
    // defined as the most recent version for the target major version.
    if (!isset($project_data['latest_version'])
        && $release['version_major'] == $target_major) {
      $project_data['latest_version'] = $version;
      $project_data['releases'][$version] = $release;
    }

    // Look for the development snapshot release for this branch.
    if (!isset($project_data['dev_version'])
        && $release['version_major'] == $target_major
        && isset($release['version_extra'])
        && $release['version_extra'] == 'dev') {
      $project_data['dev_version'] = $version;
      $project_data['releases'][$version] = $release;
    }

    // Look for the 'recommended' version if we haven't found it yet (see
    // phpdoc at the top of this function for the definition).
    if (!isset($project_data['recommended'])
        && $release['version_major'] == $target_major
        && isset($release['version_patch'])) {
      if ($patch != $release['version_patch']) {
        $patch = $release['version_patch'];
        $release_patch_changed = $release;
      }
      if (empty($release['version_extra']) && $patch == $release['version_patch']) {
        $project_data['recommended'] = $release_patch_changed['version'];
        $project_data['releases'][$release_patch_changed['version']] = $release_patch_changed;
      }
    }
    if (!isset($project_data['drupal_9_compatible'])) {
      // Calculate if compatible version of drupal 9 released.
      if (isset($release['core_compatibility']) && strpos($release['core_compatibility'], '^9') !== FALSE) {
        $project_data['drupal_9_compatible'] = TRUE;
      }
    }

  }

  // If we were unable to find a recommended version, then make the latest
  // version the recommended version if possible.
  if (!isset($project_data['recommended']) && isset($project_data['latest_version'])) {
    $project_data['recommended'] = $project_data['latest_version'];
    // No recommended version means there's a dev snapshot.
    $project_data['status'] = MIGRATION_ADVISOR_DEVELOPMENT;
    $project_data['reason'] = t('In development');
  }

  // Check to see if we need an update or not.
  // Skip security update status handling.
  // Check new Drupal core improvements, regardless of what's figured out below.
  if (migration_advisor_moved_into_core($project_data, $project)) {
    $project_data += $project_data[$project];
    $project_data['status'] = MIGRATION_ADVISOR_CORE;
    $project_data['reason'] = t('In core');
  }

  if (isset($project_data['status'])) {
    // If we already know the status, we're done.
    return;
  }

  // If we don't know what to recommend, there's nothing we can report.
  // Bail out early.
  // Commenting this out causes core to work again.
  // if (!isset($project_data['recommended'])) {
  // $project_data['status'] = UPDATE_UNKNOWN;
  // $project_data['reason'] = t('No available releases found');
  // return;
  // }.
  // Ignore dev snapshot handling.
  // Figure out the status, based on what we've seen and the install type.
  // If we were not yet able to assign a status, this project already
  // provides a stable release, so remove handling of official and dev releases.
  switch ($project_data['install_type']) {
    case 'official':
    case 'dev':
      $project_data['status'] = MIGRATION_ADVISOR_STABLE;
      $project_data['reason'] = t('Available');
      break;

    default:
      // A project without releases may be in core.
      if (migration_advisor_moved_into_core($project_data, $project)) {
        $project_data['status'] = MIGRATION_ADVISOR_CORE;
        $project_data['reason'] = t('In core');
      }
      else {
        $project_data['status'] = UPDATE_UNKNOWN;
        $project_data['reason'] = t('Invalid info');
      }
  }
}

/**
 * Return status and notice about modules that have been made obsolete.
 *
 * Assign custom upgrade information for certain modules.
 *
 * @param array $projects
 *   Array of projects from migration_advisor_calculate_project_data().
 * @param string $project
 *   Project name to check.
 *
 * @return bool
 *   TRUE if module has been made obsolete by an alternative.
 */
function migration_advisor_obsolete(array &$projects, string $project) {
  $obsolete = TRUE;

  switch ($project) {
    case 'addressfield':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'address';
      break;

    case 'admin_menu':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'admin_toolbar';
      break;

    case 'auto_nodetitle':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'auto_entitylabel';
      break;

    case 'better_formats':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'allowed_formats';
      break;

    case 'bundle_copy':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'field_tools';
      break;

    case 'calendar':
    case 'fullcalendar':
    case 'fullcalendar_create':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'fullcalendar_view';
      break;

    case 'cnr':
    case 'nodereferrer':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'cer';
      break;

    case 'colors':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'colorapi';
      break;

    case 'content_profile':
      $projects[$project]['obsolete_since'] = '7.x';
      $projects[$project]['replaced_by'][0]['name'] = 'profile2';
      break;

    case 'data_export_import':
    case 'node_export':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'content_sync';
      break;

    case 'ddf':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'conditional_fields';
      $projects[$project]['replaced_by'][1]['name'] = 'business_rules';
      $projects[$project]['replaced_by'][2]['name'] = 'field_states_ui';
      $projects[$project]['replaced_by'][3]['name'] = 'fico';
      break;

    case 'editableviews':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'views_entity_form_field';
      break;

    case 'entityreference_filter':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'verf';
      break;

    case 'entityreference_prepopulate':
    case 'nodereference_url':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'prepopulate';
      $projects[$project]['replaced_by'][1]['name'] = 'referer_to_entity_reference';
      break;

    case 'entityreference_view_widget':
    case 'references_dialog':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'entity_browser';
      $projects[$project]['replaced_by'][1]['name'] = 'inline_entity_form';
      $projects[$project]['replaced_by'][2]['name'] = 'entityconnect';
      break;

    case 'facetapi':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'facets';
      break;

    case 'fckeditor':
      $projects[$project]['obsolete_since'] = '5.x';
      $projects[$project]['replaced_by'][0]['name'] = 'ckeditor';
      break;

    case 'field_collection':
    case 'field_collection_views':
    case 'multifield':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'paragraphs';
      break;

    case 'field_conditional_state':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'conditional_fields';
      break;

    case 'form_save':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'hotkeys_for_save';
      break;

    case 'global_filter':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'simple_global_filter';
      break;

    case 'google_chart_tools':
    case 'charts_graphs':
    case 'charts_graphs_flot':
    case 'highcharts':
    case 'visualization':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'charts';
      break;

    case 'hierarchical_select':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'cshs';
      $projects[$project]['replaced_by'][1]['name'] = 'menu_link_weight';
      $projects[$project]['replaced_by'][2]['name'] = 'shs';
      break;

    case 'jqeasing':
    case 'jquery_plugin':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'libraries';
      break;

    case 'location':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'address';
      $projects[$project]['replaced_by'][1]['name'] = 'geofield';
      $projects[$project]['replaced_by'][2]['name'] = 'geocoder';
      $projects[$project]['replaced_by'][3]['name'] = 'geolocation';
      break;

    case 'megamenu':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'menu_item_extras';
      $projects[$project]['replaced_by'][1]['name'] = 'we_megamenu';
      $projects[$project]['replaced_by'][2]['name'] = 'tb_megamenu';
      $projects[$project]['replaced_by'][3]['name'] = 'ultimenu';
      $projects[$project]['replaced_by'][4]['name'] = 'simple_megamenu';
      break;

    case 'messaging':
    case 'notifications':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'courier';
      break;

    case 'menu_item_visibility':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'menu_link_content_visibility';
      break;

    case 'node_convert':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'convert_bundles';
      break;

    case 'node_clone':
    case 'replicate':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'entity_clone';
      break;

    case 'nodeaccess_userreference':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'access_by_ref';
      break;

    case 'nodewords':
      $projects[$project]['obsolete_since'] = '7.x';
      $projects[$project]['replaced_by'][0]['name'] = 'metatag';
      $projects[$project]['replaced_by'][1]['name'] = 'metatags_quick';
      break;

    case 'og':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'group';
      break;

    case 'panels_extra_styles':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'panels_extra_styles_d8';
      break;

    case 'path_redirect':
      $projects[$project]['obsolete_since'] = '7.x';
      $projects[$project]['replaced_by'][0]['name'] = 'redirect';
      break;

    case 'print':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'entity_print';
      $projects[$project]['replaced_by'][1]['name'] = 'printable';
      break;

    case 'responsive_dropdown_menus':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'responsive_menu';
      break;

    case 'search_api_db':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'search_api';
      break;

    case 'taxonomy_csv':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'term_csv_export_import';
      $projects[$project]['replaced_by'][1]['name'] = 'taxonomy_manager';
      $projects[$project]['replaced_by'][2]['name'] = 'migrate_source_csv';
      $projects[$project]['replaced_by'][3]['name'] = 'taxonomy_import';
      $projects[$project]['replaced_by'][4]['name'] = 'hti';
      $projects[$project]['replaced_by'][5]['name'] = 'term_csv_tree_import';
      break;

    case 'textformatter':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'list_formatter';
      break;

    case 'track_field_changes':
    case 'nodechanges':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'changed_fields';
      break;

    case 'user_dashboard':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'homebox';
      break;

    case 'views_arguments_extras':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'views_arg_order_sort';
      break;

    case 'views_export_xls':
    case 'views_data_export_phpexcel':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'xls_serialization';
      $projects[$project]['replaced_by'][1]['name'] = 'vbo_export';
      break;

    case 'wikitools':
      $projects[$project]['obsolete_since'] = '8.x';
      $projects[$project]['replaced_by'][0]['name'] = 'freelinking';
      break;

    default:
      $obsolete = FALSE;
  }

  return $obsolete;
}
